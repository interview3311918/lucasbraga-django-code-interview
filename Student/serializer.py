from rest_framework.serializers import ModelSerializer
from .models import CollegeUser

class CollegeUserSerializer(ModelSerializer):
    class Meta:
        model = CollegeUser
        fields = '__all__'