from rest_framework import viewsets
from .models import CollegeUser
from .serializer import CollegeUserSerializer

class StudentView(viewsets.ModelViewSet):
    queryset = CollegeUser.objects.all()
    serializer_class = CollegeUserSerializer

