from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class CollegeUser(models.Model):
    enrollment_flag = models.BooleanField(default=False)
    student_id = models.OneToOneField(User, on_delete=models.CASCADE)


